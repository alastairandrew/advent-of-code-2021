import Data.List
import System.Environment

main = do
   args <- getArgs
   let noOfDays = read $ head args :: Int
   let fileName = args !! 1
   content <- readFile fileName 
   let initialState = read $ "[" ++ content ++ "]" :: [Int]
   let stateFreq = map (\x -> (head x, length x)) $ group $ sort initialState
   putStrLn ("Initial States grouped by frequency " ++ show stateFreq)
   let curriedFish = lanternFish noOfDays -- 
   let noOfFish = sum $ map (\x -> curriedFish (fst x) * snd x) stateFreq -- calc each unique start once then just multiply by frequency
   putStrLn ("There will be " ++ show noOfFish ++ " lanternfish after " ++ show noOfDays ++ " days")

-- n - no of Days counter starts at input and ticks down
-- f - internal counter of the fish itself
lanternFish :: Int -> Int -> Int
lanternFish n f | n - f - 1 < 0 = 1 -- if we overshoot return ourself, no spawn chance.
lanternFish n f = memoizedFish (n - f - 1) 6 + memoizedFish (n - f - 1) 8 -- recursive to the next spawn step

-- Handles wrapping our functin in a memoized form
memo :: (Int -> Int -> a) -> [[a]]
memo f = map (\x -> map (f x) [0..]) [0..]

fishStore :: [[Int]]
fishStore = memo lanternFish

memoizedFish :: Int -> Int -> Int
memoizedFish x y = fishStore !! x !! y
import Data.List
import System.Environment

main = do
   args <- getArgs
   let days = read (head args) :: Int
   let fileName = head (tail args)
   content <- readFile fileName 
   let initialState = read ("[" ++ content ++ "]") :: [Int]
   putStrLn ("This was "  ++ show initialState)
   let noOfFish = length $ iterate lantern initialState !! days
   putStrLn ("There will be " ++ show noOfFish ++ " lanternfish")

lantern :: [Int] -> [Int]
lantern [] = []
lantern (x:xs) = 
   let aged = map age (x:xs)
   in aged ++ replicate (length (filter (==0) (x:xs))) 8

age :: Int -> Int
age a | a > 0 = a - 1
      | otherwise = 6

#!/usr/bin/env awk -f

NR == 1 { print $0, "(N/A - no previous measurement)" }

NR > 1 { 
    if ($0 > previous) { increasing++; print $0, "(increased)"; }
    else if ($0 < previous) { print $0, "(decreased)" }
    else { print $0, "(no change)" }
}

{ previous=$0 }

END { print "In this example, there are", increasing, "measurements that are larger than the previous measurement" }

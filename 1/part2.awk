#!/usr/bin/env awk -f

{ 
    depth[NR]=$0;
    delete depth[NR-4];
}

NR == 3 { print (depth[NR] + depth[NR-1] + depth[NR-2]), "(N/A - no previous sum)" }

NR > 3 {
   prev3=depth[NR-1] + depth[NR-2] + depth[NR-3];
   curr=depth[NR] + depth [NR-1] + depth[NR-2];
   if (curr > prev3) { increasing++; print curr, "(increased)"; }
   else if (curr == prev3) { print curr, "(no change)" }
   else { print curr, "(decreased)" } 
 }

END { print "In this example, there are", increasing, "sums that are larger than the the previous sum." }

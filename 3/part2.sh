#!/usr/bin/env bash

inputFile=$1

function filter() {
    local bitPosition=$1; local pattern=$2; local defaultValue=$3; local mostCommonBit=$4

    noOfMatchingLines=$(grep -c "^$pattern" $inputFile)
    if [ "$noOfMatchingLines" -eq "1" ]
    then
        awk -v f="^$pattern" '$0 ~ f { print "obase=10;ibase=2;", $0 }' $inputFile | bc 
        return
    fi

    read -r count bit <<<$(grep "^$pattern" $inputFile | cut -c $bitPosition | sort | uniq -c | sort -r)

    # Was the most common count only half the file length? (i.e. 
    if [ $((count * 2)) -eq "$noOfMatchingLines" ]
    then
        pattern+="$defaultValue" # 
    else
      if [ $mostCommonBit -eq "1" ]
      then 
          pattern+=$bit;
      else
          pattern+=$((1 - $bit))
      fi
    fi

    bitPosition=$(($bitPosition + 1))
    filter $bitPosition $pattern $defaultValue $mostCommonBit
}

oxygenGeneratorRating=$(filter 1 "" 1 1)
cO2ScrubberRating=$(filter 1 "" 0 0)

lifeSupportRating=$(($oxygenGeneratorRating * $cO2ScrubberRating))
echo "The life support rating is the oxgen generator rating ($oxygenGeneratorRating) multiplied by the CO2 scrubber rating ($cO2ScrubberRating) = $lifeSupportRating"



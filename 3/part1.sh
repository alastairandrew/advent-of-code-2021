#!/usr/bin/env bash


expression=$(./diagnostics.awk $1)

echo "gamma rate * epsilon rate = $expression"


powerConsumption=$(echo "obase=10;ibase=2;$expression" | bc)
echo "The power consumption of the submarine is $powerConsumption"

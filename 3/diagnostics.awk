#!/usr/bin/env awk -f
BEGIN { FS = ""; gamma=""; epsilon=""; }

{ for(i=1; i <= NF; ++i) if($(i) > 0) count[i]++ }

END { 
    for(i=1; i <= NF; i++) {
      if  (count[i] > (NR / 2)) { gamma = (gamma "1"); epsilon = (epsilon "0"); }
      else { gamma = (gamma "0"); epsilon = (epsilon "1"); }
    } 

    print gamma, "*", epsilon;
}

package main

import (
	"aoc/4/pkg/bingo"
	"bufio"
	"fmt"
	"log"
	"os"
)

func main() {
	fileName := os.Args[1]
	file, err := os.Open(fileName)

	if err != nil {
		log.Fatalf("failed to open")
	}

	// The bufio.NewScanner() function is called in which the
	// object os.File passed as its parameter and this returns a
	// object bufio.Scanner which is further used on the
	// bufio.Scanner.Split() method.
	scanner := bufio.NewScanner(file)

	// The bufio.ScanLines is used as an
	// input to the method bufio.Scanner.Split()
	// and then the scanning forwards to each
	// new line using the bufio.Scanner.Scan()
	// method.
	scanner.Split(bufio.ScanLines)

	scanner.Scan()
	firstLine := scanner.Text()
	fmt.Println("First line " + firstLine)

	var inputInts = bingo.ParseInput(firstLine, ",")

	fmt.Println("Created ", len(inputInts), " long input file")

	noOfBoards := 0

	var boards = make([]bingo.BingoBoard, 0)

	for scanner.Scan() {
		var currentLine = scanner.Text()
		if len(currentLine) > 0 {
			var data = bingo.ParseInput(scanner.Text(), " ")
			boards[noOfBoards-1].AddRow(bingo.Row{data})
		} else {
			noOfBoards++
			fmt.Println("New Board time!")
			boards = append(boards, bingo.BingoBoard{Id: noOfBoards, DrawnValues: make(map[int]bool), ValuePositions: make(map[int]bingo.ValueIndices)})
		}
	}
	// The method os.File.Close() is called
	// on the os.File object to close the file
	file.Close()

	fmt.Println("Problem has ", noOfBoards, " bingo boards")

	fmt.Println(inputInts)

	var lastWinningBoard int
	var lastWinningDraw int
	alreadyWon := make(map[int]bool)

	for _, input := range inputInts {
		for _, board := range boards {
			if _, hasWon := alreadyWon[board.Id]; hasWon {
				continue
			}

			if board.Update(input) {
				alreadyWon[board.Id] = true
				lastWinningBoard = board.Id
				lastWinningDraw = input
			}
		}
	}

	lastBoard := boards[lastWinningBoard-1]
	boardScore := lastBoard.Score()
	fmt.Println("The last board to win was", lastBoard.Id, " with a score", boardScore, " from value", lastWinningDraw, "=", boardScore*lastWinningDraw)
}

package main

import (
	"aoc/4/pkg/bingo"
	"bufio"
	"fmt"
	"log"
	"os"
)

func main() {
	fileName := os.Args[1]
	file, err := os.Open(fileName)

	if err != nil {
		log.Fatalf("failed to open")
	}
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)

	// Manually read the first header line
	scanner.Scan()
	firstLine := scanner.Text()

	var inputInts = bingo.ParseInput(firstLine, ",")

	noOfBoards := 0
	var boards = make([]bingo.BingoBoard, 0)

	for scanner.Scan() {
		var currentLine = scanner.Text()
		if len(currentLine) > 0 {
			var data = bingo.ParseInput(scanner.Text(), " ")
			boards[noOfBoards-1].AddRow(bingo.Row{Values: data})
		} else {
			noOfBoards++
			boards = append(boards, bingo.BingoBoard{Id: noOfBoards, DrawnValues: make(map[int]bool), ValuePositions: make(map[int]bingo.ValueIndices)})
		}
	}
	file.Close()

	for drawNo, input := range inputInts {
		for _, board := range boards {
			if board.Update(input) {
				boardScore := board.Score()
				fmt.Println("Board", board.Id, "wins:", boardScore, "*", input, "=", boardScore*input)
				return
			}
		}
		fmt.Println("No winners after", drawNo, "draws")
	}
}

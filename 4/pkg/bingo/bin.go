package bingo

import (
	"strconv"
	"strings"
)

type Row struct {
	Values []int
}

type ValueIndices struct {
	row, column int
}

type BingoBoard struct {
	Id             int
	rows           []Row
	cols           []Row
	DrawnValues    map[int]bool
	ValuePositions map[int]ValueIndices
}

func ParseInput(line string, separator string) []int {
	parsed := make([]int, 0)
	for _, value := range strings.Split(line, separator) {
		var iValue, err = strconv.Atoi(value)
		if err == nil {
			parsed = append(parsed, iValue)
		}
	}
	return parsed
}

func (b *BingoBoard) Update(ball int) bool {
	b.DrawnValues[ball] = true
	pos, ok := b.ValuePositions[ball]
	if !ok {
		return false
	}

	var match = true
	for _, v := range b.rows[pos.row].Values {
		if _, found := b.DrawnValues[v]; !found {
			match = false
			break
		}
	}

	if match {
		return true
	}

	match = true
	for _, v := range b.cols[pos.column].Values {
		if _, found := b.DrawnValues[v]; !found {
			match = false
			break
		}
	}
	return match
}

func (b *BingoBoard) AddRow(r Row) {
	b.rows = append(b.rows, r)

	for i, v := range r.Values {
		b.ValuePositions[v] = ValueIndices{len(b.rows) - 1, i}
	}

	if len(b.rows) == 5 {
		for i := 0; i < 5; i++ {
			values := make([]int, 5)
			for j := 0; j < len(b.rows); j++ {
				values[j] = b.rows[j].Values[i]
			}

			col := Row{values}

			b.cols = append(b.cols, col)
			//fmt.Println("Cols", b.cols)
		}
	}
}

func (b *BingoBoard) Score() int {
	acc := 0
	for _, r := range b.rows {
		for _, v := range r.Values {
			if _, ok := b.DrawnValues[v]; !ok {
				acc += v
			}
		}
	}
	return acc
}

#! /usr/bin/env awk -f
#
$1 == "forward" { horizontal += $2 }
$1 == "up" { depth -= $2 }
$1 == "down" { depth += $2 }

END { print "After following these instructions, you would have a horizon position of", horizontal,
            "and a depth of", depth, ". (Multiplying these together produces", depth * horizontal, ".)" }

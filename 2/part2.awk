#! /usr/bin/env awk -f
#
$1 == "forward" { horizontal += $2; depth += (aim * $2) }
$1 == "up" { aim -= $2 }
$1 == "down" { aim += $2 }

END { print "After following these new instructions, you would have a horizontal position of", horizontal, "and a depth of", depth,
    ". (Multiplying these produces", depth * horizontal, ".)" }

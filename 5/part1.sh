#! /usr/bin/env bash

declare -A grid

function print {
    local overlapCount=0
    for j in $(seq 0 $maxY); do
        for i in $(seq 0 $maxX); do
            local key="($i,$j)"
            if [ -v "grid[$key]" ]; then 
                local overlap=${grid[$key]}
                echo -n $overlap
                if [ "$overlap" -gt 1 ]; then
                    overlapCount=$((overlapCount + 1))
                fi
            else
                echo -n "."
            fi
        done

        echo ""
    done;

    echo "There are $overlapCount points where at least two lines overlap"
}


function visit() {
    local x=$1
    local y=$2

    if [ "$x" -gt "$maxX" ]; then
        maxX=$x
    fi

    if [ "$y" -gt "$maxY" ]; then
        maxY=$y
    fi


    local key="($x,$y)"
    if [ -v "grid[$key]" ]; then
        grid[$key]=$((${grid[$key]} + 1))
    else 
        grid[$key]=1
    fi
}

maxX=0
maxY=0

while read -r x1 y1 x2 y2; do 
    if [ "$x1" -eq "$x2" ]; then
        for y in $(seq $y1 $y2); do
            visit $x1 $y
        done    
    else
        for x in $(seq $x1 $x2); do
            visit $x $y1
        done 
    fi
done < <(sed 's/ -> /,/g' $1 | awk -v FS="," '$1 == $3 || $2 == $4 { print $1, $2, $3, $4 }')

print 



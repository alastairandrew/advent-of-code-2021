#! /usr/bin/env bash

declare -A grid

function print {
    local overlapCount=0
    for j in $(seq 0 $maxY); do
        for i in $(seq 0 $maxX); do
            local key="($i,$j)"
            if [ -v "grid[$key]" ]; then 
                local overlap=${grid[$key]}
                echo -n "$overlap "
                if [ "$overlap" -gt 1 ]; then
                    overlapCount=$((overlapCount + 1))
                fi
            else
                echo -n ". "
            fi
        done
        echo ""
    done
    echo "There are $overlapCount points where at least two lines overlap"
}

function visit() {
    local x=$1
    local y=$2

    if [ $x -gt $maxX ]; then
        maxX=$x
    fi

    if [ $y -gt $maxY ]; then
        maxY=$y
    fi

    local key="($x,$y)"
    if [ -v "grid[$key]" ]; then
        grid[$key]=$((${grid[$key]} + 1))
    else 
        grid[$key]=1
    fi
}

maxX=0
maxY=0

while read -r x1 y1 x2 y2; do 
    x=$x1
    y=$y1
    while [ $x -ne $x2 -o $y -ne $y2 ]; do
        visit $x $y
        if [ $x -lt $x2 ]; then
            x=$((x + 1))
        elif [ $x -gt $x2 ]; then
            x=$((x - 1))
        fi

        if [ $y -lt $y2 ]; then
            y=$((y + 1))
        elif [ $y -gt $y2 ]; then
            y=$((y - 1))
        fi
    done
    visit $x2 $y2
    
done < <(sed 's/ -> /,/g' $1 | tr ',' ' ')

print 
:
